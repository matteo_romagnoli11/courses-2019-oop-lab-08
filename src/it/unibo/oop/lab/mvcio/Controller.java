package it.unibo.oop.lab.mvcio;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import it.unibo.oop.lab.iogui.BadIOGUI;

/**
 * 
 */
public class Controller {
    
    private File file;
    private static final String PATH = System.getProperty("user.home")
            + System.getProperty("file.separator")
            +"output.txt";
    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     */
    public Controller() {
        this.file = new File(PATH);
    }
    
    public void setFile(File f) {
        this.file = f;
    }
     /* 2) A method for getting the current File
     */
    public File getFile() {
        return this.file;
    }
     /* 3) A method for getting the path (in form of String) of the current File
     */
    public String getPath() {
        return this.file.getAbsolutePath();
    }
     /* 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     */
    public void writeOnFile(String input) {
        try (PrintStream ps = new PrintStream(this.file.getAbsolutePath())) {
            ps.print(input);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
     /* 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */

}
